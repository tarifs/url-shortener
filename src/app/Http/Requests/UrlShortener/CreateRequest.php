<?php

namespace App\Http\Requests\UrlShortener;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required',
            'hit_limit' => 'required|integer',
            'hit_time_limit' => 'required|integer',
            'block_time' => 'required|integer',
            'expire_time' => 'required',
            'expire_date' => 'required_if:expire_time,==,yes',
        ];
    }
}
