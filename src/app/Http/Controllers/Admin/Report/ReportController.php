<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\UrlTracker;
use App\Models\User;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private function filterDate($request) {
        if ($request->from_to) {
            $request_date = explode(" - ", $request->input('from_to', ""));
            $date = [$request_date[0].' '.'00:00:00', $request_date[1].' '.'23:59:59'];
            if(count($request_date) != 2)
            {
                $date = [now()->subDays(29)->format("Y-m-d").' '.'00:00:00', now()->format("Y-m-d").' '.'23:59:59'];
            }
        }else {
            $date = [now()->subDays(29)->format("Y-m-d").' '.'00:00:00', now()->format("Y-m-d").' '.'23:59:59'];
        }

        return $date;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        set_page_meta('Reports');
        $date = $this->filterDate($request);
        $user = User::where('id', auth()->user()->id)->with('url_shorteners')->first();
        $url_ids = $user->url_shorteners->pluck('id')->toArray();
        $report = UrlTracker::whereIn('url_id', $url_ids)->whereBetween('created_at', $date);
        if($request->most_visited) {
            $report->where(function($q) use($request) {
                try {
                $data = UrlTracker::select($request->most_visited)->groupBy($request->most_visited)
                ->orderByRaw('COUNT(*) DESC')
                ->limit(1)
                ->first();

                if($data) {
                    $result = $data->toArray();
                    $q->where($request->most_visited, $result[$request->most_visited]);
                }
                }catch(\Exception $e) {}
    
            });
        }
        $reports = $report->latest()->get();

        
        return view('admin.reports.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
