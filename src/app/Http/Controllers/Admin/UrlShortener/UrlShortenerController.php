<?php

namespace App\Http\Controllers\Admin\UrlShortener;

use App\Http\Controllers\Controller;
use App\Http\Requests\UrlShortener\CreateRequest;
use App\Models\UrlShortner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use URL;

class UrlShortenerController extends Controller
{
    private function generateShortKey() {
        $result = base_convert(time(), 10, 36);
        $check_key = UrlShortner::where('short_key', $result)->first();
        if(!empty($check_key)) {
            $this->generateShortKey();
        }
        return $result;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_page_meta('Url Shortener List');
        $url_shorteners = UrlShortner::where('user_id', auth()->user()->id)->get();
        return view('admin.url_shortener.index', compact('url_shorteners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_page_meta('Url Shortener Create');
        return view('admin.url_shortener.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
       $url = UrlShortner::where(['url' => $request->url, 'user_id' => auth()->user()->id])->first();
       if(empty($url)) {
        $short_key = $this->generateShortKey();
        $expire_date = null;
        if($request->expire_date) {
            $expire_date = Carbon::parse($request->expire_date)->format('Y-m-d');
        }
        $url = UrlShortner::create([
            'url' => $request->url,
            'user_id' => auth()->user()->id,
            'short_key' => $short_key,
            'hit_limit' => $request->hit_limit,
            'hit_time_limit' => $request->hit_time_limit,
            'block_time' => $request->block_time,
            'expire_date' => $expire_date,
        ]);

        //generate qrcode
        $renderer = new ImageRenderer(
            new RendererStyle(400),
            new ImagickImageBackEnd()
        );
        $writer = new Writer($renderer);
        $writer->writeFile(URL::to('/'.$short_key), public_path('qrcode/'.$short_key.'.png'));
       }

       return redirect()->route('admin.url-shorteners.show', $url->id);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        set_page_meta('Url Shortener Show');
        $url = UrlShortner::find($id);
        return view('admin.url_shortener.show', compact('url'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UrlShortner::destroy($id);
        flash('Successfully Deleted!')->success();
        return back();
    }
}
