<?php

namespace App\Http\Controllers;

use App\Models\UrlShortner;
use App\Models\UrlTracker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Http;
use Jenssegers\Agent\Agent;
use Facades\Spatie\Referer\Referer;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    private function getUserIpAddr() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = null;
        return $ipaddress;
     }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function shortUrl(Request $request, $short_key)
    {
        $user_ip = $this->getUserIpAddr();
        if(Cache::has('block_ip')) {
            if(Cache::get('block_ip') == $user_ip) abort(403);
        }
        $url = UrlShortner::where('short_key', $short_key)->first();
        if(!empty($url)) {
            if($url->expire_date) {
            $today_date = Carbon::now()->format('Y-m-d');
            if($today_date > $url->expire_date) abort(404);
            }

            $executed = RateLimiter::attempt(
                'send-message:'. 1,
                $url->hit_limit,
                function() {},
                $url->hit_time_limit * 60
            );
             
            if (! $executed) {
                Cache::put('block_ip', $user_ip, $url->block_time * 60);
                abort(429);
            }

            if(!is_null($user_ip)) {
                $user_location = Http::get('http://ip-api.com/json/'.$user_ip);
            }

            $agent = new Agent();
            $device = $agent->device();
            $browser = $agent->browser();
            $platform = $agent->platform();
            $referer = Referer::get();
            

            UrlTracker::create([
                'url_id' => $url->id,
                'url' => $url->url,
                'ip' => $user_ip,
                'location' => $user_location['status'] != 'fail' ? $user_location['city'].', '.$user_location['country'] : null,
                'lat' => $user_location['status'] != 'fail' ? $user_location['lat'] : null,
                'lon' => $user_location['status'] != 'fail' ? $user_location['lon'] : null,
                'os' => $platform,
                'device' => $device,
                'browser' => $browser,
                'referer' => $referer? $referer : null
            ]);

            return redirect($url->url);
        }
    }
}
