<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlTracker extends Model
{
    use HasFactory;
    protected $fillable = ['url_id', 'url', 'ip', 'location', 'os', 'device', 'referer', 'lat', 'lon', 'browser'];
}
