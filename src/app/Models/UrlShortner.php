<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlShortner extends Model
{
    use HasFactory;
    protected $fillable = ['url', 'short_key', 'hit_limit', 'hit_time_limit', 'block_time', 'expire_date', 'user_id'];
}
