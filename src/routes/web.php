<?php

use App\Http\Controllers\Admin\Dashboard\DashboardController;
use App\Http\Controllers\Admin\Report\ReportController;
use App\Http\Controllers\Admin\UrlShortener\UrlShortenerController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes([
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::prefix('admin')->as('admin.')->middleware('auth')->group(function () {
    // DASHBOARD
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // URL SHORTENER
    Route::resource('/url-shorteners', UrlShortenerController::class);
    // REPORTS
    Route::resource('/reports', ReportController::class);
});

Route::get('/{short_key}', [HomeController::class, 'shortUrl'])->name('short_url');
