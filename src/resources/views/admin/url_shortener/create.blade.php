@extends('admin.layouts.master')
@section('content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Add Url Shortener</h4>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body m-t-10">
                    <form action="{{ route('admin.url-shorteners.store') }}" method="post">
                        @csrf
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="name" class="col-lg-2 col-sm-12 col-form-label">Url</label>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="url" value="{{ old('url') }}" class="form-control" name="url" placeholder="Enter your url" autofocus required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-lg-2 col-sm-12 col-form-label">Hit</label>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="number" value="{{ old('hit_limit') }}" class="form-control" name="hit_limit" placeholder="Enter url hit limit" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-lg-2 col-sm-12 col-form-label">Per Miniute</label>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="number" value="{{ old('hit_time_limit') }}" class="form-control" name="hit_time_limit" placeholder="Enter url hit per miniute" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-lg-2 col-sm-12 col-form-label">Block Time</label>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="number" value="{{ old('block_time') }}" class="form-control" name="block_time" placeholder="Enter block miniute" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-lg-2 col-sm-12 col-form-label">Expire Date</label>
                                <div class="col-lg-6 col-sm-12">
                                    <input type="radio" name="expire_time" onclick="checkExpire('yes')" value="yes"> Yes &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="expire_time" checked onclick="checkExpire('no')" value="no"> No
                                    <br>
                                    <br>
                                    <input type="date" value="{{ old('expire_date') }}" class="form-control expire_date d-none" name="expire_date" placeholder="Enter expire date">
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button class="btn btn-primary waves-effect waves-lightml-2" type="submit">
                                        <i class="fa fa-save"></i> Submit
                                    </button>
                                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.url-shorteners.index') }}">
                                        <i class="fa fa-times"></i> Cancel
                                    </a>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        function checkExpire(val) {
           if(val == 'yes') {
               $('.expire_date').removeClass('d-none');
               $('.expire_date').attr('required', 'true');
           }else {
            $('.expire_date').addClass('d-none');
            $('.expire_date').prop('required',false);
           }
        }
    </script>
@endpush