@extends('admin.layouts.master')
@section('content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Url Shortener List</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12 pl-0 mb-2">
                        <a href="{{route('admin.url-shorteners.create')}}" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> Create Short Url</a>
                    </div>
                    <br>
                    <br>
                    <div class="dt-responsive table-responsive">
                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th>#SL</th>
                                <th>Destination</th>
                                <th>Hit Limit</th>
                                <th>Per Minute</th>
                                <th>Block Minute</th>
                                <th>Expire Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if($url_shorteners)
                                @foreach($url_shorteners as $key => $url_shortener)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $url_shortener->url }}</td>
                                        <td>{{ $url_shortener->hit_limit }}</td>
                                        <td>{{ $url_shortener->hit_time_limit }}</td>
                                        <td>{{ $url_shortener->block_time }}</td>
                                        <td>{{ $url_shortener->expire_date? Carbon\Carbon::parse($url_shortener->expire_date)->format('M d Y') : 'N/A' }}</td>
                                        <td class="text-center">
                                            <a href="#" onclick="return false;" class="dropdown-toggle dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item btn btn-sm btn-info" href="{{route('admin.url-shorteners.show', $url_shortener->id)}}"><i class="fa fa-eye"></i> show</a>
                                                <form action="{{ route('admin.url-shorteners.destroy', $url_shortener->id) }}" id="delete-form-{{ $url_shortener->id }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="dropdown-item btn btn-sm btn-danger" onclick="return makeDeleteRequest(event, {{ $url_shortener->id }})" type="submit" title="Delete">
                                                        <i class="fas fa-trash"></i>
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection
@push('style')
    @include('admin.includes.styles.datatable')
@endpush

@push('script')
    @include('admin.includes.scripts.datatable')
@endpush