@extends('admin.layouts.master')
@section('content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Your short url</h4>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body m-t-10">
                    <a href="{{ URL::to('/'.$url->short_key) }}" target="_blank" rel="noopener noreferrer">{{ URL::to('/'.$url->short_key) }}</a>
                    <br>
                    <img src="{{  URL::to('/').'/qrcode/'.$url->short_key.'.png' }}" alt="no-qrcode" srcset="" width="20%" height="20%">
                </div>
            </div>
        </div>
    </div>

@endsection