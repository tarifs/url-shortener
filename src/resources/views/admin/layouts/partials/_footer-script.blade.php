<!-- Required Jquery -->
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/jquery-slimscroll/jquery.slimscroll.js"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/modernizr/modernizr.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/modernizr/feature-detects/css-scrollbars.js"></script>
<!-- Chart js -->
<script type="text/javascript" src="{{ asset('/') }}adminity/files/bower_components/chart.js/dist/Chart.js"></script>
<!-- amchart js -->
<script src="{{ asset('/') }}adminity/files/assets/pages/widget/amchart/amcharts.js"></script>
<script src="{{ asset('/') }}adminity/files/assets/pages/widget/amchart/serial.js"></script>
<script src="{{ asset('/') }}adminity/files/assets/pages/widget/amchart/light.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{ asset('/') }}adminity/files/assets/js/SmoothScroll.js"></script>
<script src="{{ asset('/') }}adminity/files/assets/js/pcoded.min.js"></script>
<script src="{{ asset('/') }}adminity/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{ asset('/') }}adminity/files/assets/js/vartical-layout.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/assets/pages/dashboard/analytic-dashboard.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}adminity/files/assets/js/script.js"></script>

<!-- Sweet-Alert  -->
<script src="{{ URL::asset('adminity/files/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
    function makeDeleteRequest(event, id) {
        event.preventDefault();
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: "btn btn-danger mr-2"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons
            .fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            })
            .then(result => {
                console.log(id);
                if (result.value) {
                    let form_id = $("#delete-form-" + id);
                    $(form_id).submit();
                }
            });
    }
</script>

{{-- <script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script> --}}

@stack('script')
