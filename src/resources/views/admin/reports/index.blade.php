@extends('admin.layouts.master')
@section('content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">All Reports</h4>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <form method='get'>
                                <div class="input-group">
                                    <input type="text" name="from_to" class="form-control mr-2" id="date_filter">
                                    <input type="submit" class="btn btn-primary" value="Filter">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form method='get'>
                                <div class="input-group">
                                    <select class="form-control mr-2" name="most_visited">
                                        <option value="location" {{ request('most_visited') == 'location'? 'selected' : '' }} >Location</option>
                                        <option value="os" {{ request('most_visited') == 'os'? 'selected' : '' }} >OS</option>
                                        <option value="device" {{ request('most_visited') == 'device'? 'selected' : '' }} >Device</option>
                                        <option value="browser" {{ request('most_visited') == 'browser'? 'selected' : '' }} >Browser</option>
                                      </select>
                                    <input type="submit" class="btn btn-primary" value="Most Visited">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <br>
                    <br>
                    <div class="dt-responsive table-responsive">
                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th>Ip Address</th>
                                <th>Location</th>
                                <th>Os</th>
                                <th>Device</th>
                                <th>Browser</th>
                                <th>Referer</th>
                                <th>Lat</th>
                                <th>Lon</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if($reports)
                                @foreach($reports as $key => $report)
                                    <tr>
                                        <td>{{ $report->ip }}</td>
                                        <td>{{ $report->location }}</td>
                                        <td>{{ $report->os }}</td>
                                        <td>{{ $report->device }}</td>
                                        <td>{{ $report->browser }}</td>
                                        <td>{{ $report->referer }}</td>
                                        <td>{{ $report->lat }}</td>
                                        <td>{{ $report->lon }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div>

@endsection
@push('style')
    @include('admin.includes.styles.datatable')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endpush

@push('script')
    @include('admin.includes.scripts.datatable')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        let searchParams = new URLSearchParams(window.location.search)
        let dateInterval = searchParams.get('from_to');
        let start = moment().subtract(29, 'days');
        let end = moment();

        if (dateInterval) {
            dateInterval = dateInterval.split(' - ');
            start = dateInterval[0];
            end = dateInterval[1];
        }

        $('#date_filter').daterangepicker({
            "showDropdowns": true,
            "showWeekNumbers": true,
            "alwaysShowCalendars": true,
            startDate: start,
            endDate: end,
            locale: {
                format: 'YYYY-MM-DD',
                firstDay: 1,
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                'All time': [moment().subtract(30, 'year').startOf('month'), moment().endOf('month')],
            }
        });
    </script>

@endpush