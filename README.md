### Run docker command

```
docker-compose up -d
```

```
docker-compose exec php sh
```

```
composer update
```

Copy the .env.example file in the src directory and rename it to .env

```
php artisan key:generate
```

```
php artisan migrate
```

### Visit Site
http://localhost:8888
```

### Phpmyadmin url
http://localhost:8088
```
